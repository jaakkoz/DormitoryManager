/*
var userR = document.getElementById('usernameError');
var pswR = document.getElementById('passwordError');
var confirmR = document.getElementById('confirmPswError');

function clearErrorText(){
  userR.style.display = 'none';
  pswR.style.display = 'none';
  confirmR.style.display = 'none';
}

$(document).ready(function(){
  var currencyFileHref = hostUrl + "/lab/getlabtype";
  // 先进行请求.请求实验室相关数据 并显示在注册列表中.
  request(null,currencyFileHref,function(data){
    var showList = document.getElementById("labList");
    var labData = data.data;
    var liStr = '';
    for(var i in labData){
      if(!labData[i].open){
        liStr += "<option disabled='disabled'>" + labData[i].labid + " - " + labData[i].labname + "</option>";
      }else {
        liStr += "<option>" + labData[i].labid + " - " + labData[i].labname + "</option>";
      }
    }
    showList.innerHTML = liStr;
  },
  function(error){
    console.log(error);
  });

  // 检查注册信息合法性 .
  function checkInput(username,password,confirmpassword,labSelect){
    var userNameReg = /^([a-zA-Z0-9]{8,16})$/; // 8-16位学工号
    if(!userNameReg.test(username)){
      userR.innerHTML = "用户名不合法，请重新输入！";
      userR.style.display = 'block';
      return ;
    }else {
      userR.style.display = '';
    }
    var pswReg = /^([a-zA-Z0-9]{6,16})$/;
    if(!pswReg.test(password)){
      pswR.innerHTML = "密码格式不正确，请重新输入！";
      pswR.style.display = 'block';
      return ;
    }else {
      pswR.style.display = '';
    }
    if(password !== confirmpassword){
      confirmR.innerHTML = "两次输入密码不一致，请检查后输入！";
      confirmR.style.display = 'block';
      return ;
    }else {
      confirmR.style.display = '';
    }
  }

  // 确认提交事件
  document.getElementsByClassName('submitForm')[0]
          .onclick = function registerBtn(){
    var username = document.getElementById('userName').value,
        password = document.getElementById('psw').value,
        confirmpassword = document.getElementById('confirmPsw').value,
        labSelect = document.getElementById('labList');

    checkInput(username,password,confirmpassword,labSelect);

    var index = labSelect.selectedIndex;
    var selectLabStr = labSelect.options[index].text;
    var labid = selectLabStr.slice(0,1);

    var el = {};
    el.username = username,el.password = password,el.labid = labid;
    console.log(el);
    request(el,'/labManager/account/register',function(data){
      console.log(data.message);
    },
    function(error){
      console.log(error);
    });
  }
});
*/

// 轮播图

window.onload = function () {

  var container = document.getElementById('container');
  var list = document.getElementById('list');
  var pics = list.getElementsByTagName('img');
  var pic = pics[0];
  var buttons = document.getElementById('buttons').getElementsByTagName('span');
  var prev = document.getElementById('prev');
  var next = document.getElementById('next');
  var index = 1;

  // 移动
  function move (offset) {
    var oldLeft = parseInt(list.style.left);  // 原始位置
    var newLeft = oldLeft + offset;   //新位置
    var time = 500;   //位移总时间
    var interval = 10;  //位移间隔时间

    var speed = offset / (time/interval);

    var timer = setInterval(go,interval);
    function go () {
      var currentLeft = parseInt(list.style.left);
      if ((speed<0 && currentLeft>newLeft) || (speed>0 && currentLeft<newLeft)) {
        list.style.left = parseInt(list.style.left) + speed + 'px';
      }else {
        clearInterval(timer);
        if (newLeft > 0) {
          newLeft = - (pics.length - 3) * pic.width;
        } else if (newLeft < - (pics.length - 3) * pic.width) {
          newLeft = 0;
        }
        list.style.left = newLeft + 'px';
      }
    }
  }

  // 圆点导航显示
  function showButtons () {
    for(let i = 0 ; i < buttons.length ; i++){
      if (buttons[i].className == 'on') {
        buttons[i].className = '';
        break;
      }
    }
    buttons[index - 1].className = 'on';
  }

  // 下一张
  next.onclick = function () {
    move(-pic.width);
    index++;
    if (index > buttons.length) {
      index = 1;
    }
    showButtons();
  }

  //上一张
  prev.onclick = function () {
    move(pic.width);
    index--;
    if (index < 1) {
      index = buttons.length;
    }
    showButtons();
  }

  // 圆点导航切换
  for(let i = 0; i < buttons.length ; i++){
    buttons[i].id = i+1;
    buttons[i].onclick = function () {
      // 偏移量 = 点击位置 - 当前位置
      var offLen = this.id - index;
      if (!offLen) {
        return;
      }
      move(-offLen * pic.width);
      index = this.id;
      showButtons();
    }
  }

  var timer;
  // 自动播放
  function autoPlay () {
    timer = setInterval(next.onclick,3000);
  }
  function stop () {
    clearInterval(timer);
  }

  var container = document.getElementById('container');
  autoPlay();
  container.onmouseover = stop;
  container.onmouseout = autoPlay;
}