var hostUrl = 'http://39.108.175.18:8080/api/';

function getAllInfo (urlto) {
	var data;
	$.ajax({
		type:"GET",
		url: hostUrl + urlto,
		async: false,
		dataType: 'json',
		success: function (result,status,xhr) {
			data = result;
		},
		error: function(xhr){
			console.log("错误提示： " + xhr.status + " " + xhr.statusText);
		}
	})
	return data;
}

// 设置 innerHTML
function setValue (el,value) {
	el.innerHTML = value;
}

// ***bug：无法通过 addTd() 方法 添加列
// 添加行
function addTd (tableInnerHTML,value) {
	tableInnerHTML = tableInnerHTML + '<td>' + value + '</td>';
}

$(document).ready(function(){
	var userId = localStorage.username;	// 用户名
	var data = getAllInfo('student/allInfo.do?rows=20&page=1&stuNum=' + userId)
				.data.paginationList[0];	//该用户全部信息
	var userInfo = $('.navbar-right span')[0];	//学生个人信息
	var stuName = $('#personInfo #stuName')[0];	//学生姓名
	var stuNum = $('#personInfo #stuNum')[0];	//学生学号
	var buildName = $('#personInfo #buildName')[0];	//所在楼栋
	var dormNum = $('#personInfo #dormNum')[0];	//所在宿舍
	var bedNum = $('#personInfo #bedNum')[0];	//所在床位

	setValue(userInfo,' 学生' + userId);
	setValue(stuName,data.student.stuName);
	setValue(stuNum,data.student.stuNum);
	setValue(buildName,data.buildName);
	setValue(dormNum,data.dormitNum);
	setValue(bedNum,data.student.bedNum);
	
	var dormData = getAllInfo('student/allInfo.do?rows=20&page=1&dormitNum=' + data.dormitNum)
					.data.paginationList;

	// 宿舍成员信息表格
	var table = $('#dormMember tbody')[0];
	var tableInnerHTML = '';
	for (var i = 0,len = dormData.length; i < len; i++) {
		tableInnerHTML += '<tr>' ;
		tableInnerHTML = tableInnerHTML + '<td>' + dormData[i].student.stuNum + '</td>';
		tableInnerHTML = tableInnerHTML + '<td>' + dormData[i].student.stuName + '</td>';
		tableInnerHTML = tableInnerHTML + '<td>' + dormData[i].student.bedNum + '</td>';
		tableInnerHTML = tableInnerHTML + '<td>' + dormData[i].student.sex + '</td>';
		tableInnerHTML = tableInnerHTML + '<td>' + dormData[i].collegeName + '</td>';
		tableInnerHTML = tableInnerHTML + '<td>' + dormData[i].departName + '</td>';
		tableInnerHTML += '</tr>';
	}
	console.log(tableInnerHTML);
	table.innerHTML = tableInnerHTML;

}());