let page = 1;
let pageSum = 10;
let infoData = [];
let currencyFileHref;
let delFileHerf;
let nowpage = document.getElementById("nowPage");
let totalsum = document.getElementById("totalSum");
let laquo = document.getElementById("laQuo");
let raquo = document.getElementById("raQuo");
let tablebody = document.getElementById("tableBody");
let btn_modify = document.getElementById('btn_modify');
let btn_del = document.getElementById('btn_del');

let el = {};
el.rows=5;

let find = {};
let del = {};

//删除
function btnClikToDel(){
///student.do
    // $('#myModal').modal('hide');
    //console.log('hide');
    let checkArr = document.getElementsByName('test');
    let s=''; 
    for(let i = 0; i < checkArr.length; i ++){ 
        if(checkArr[i].checked) 
            s+=checkArr[i].value+','; //如果选中，将value添加到变量s中 
    }
    //那么现在来检测s的值就知道选中的复选框的值了 
    s==''?alert('你还没有选择任何内容！'):console.log(s = s.slice(0,s.length-1)); 

    delFileHerf = hostUrl + '/student.do?ids=' + s;

    requestToDel(delFileHerf,function(data){
        alert("已删除！！！");
    },
    function(error){
        console.log(error);
    });

    cleanTableRow();

    el.page=page;
    request(el,currencyFileHref,function(data){
        infoData = data.data.paginationList;
        pageSum = Math.ceil(data.data.total/el.rows);
        for(let i=0; i<infoData.length; i++){
            creatTableRow(infoData[i]);
        }
    },
    function(error){
        console.log(error);
    });

    nowpage.innerHTML = page;
    totalsum.innerHTML = pageSum;
}

//查找按钮
function btnClikToFind(){
    page = 1;

    el.stuName = document.getElementById("stuname").value,
    el.stuNum = document.getElementById("stdnum").value,
    el.departName = document.getElementById("departname").value,
    el.collegeName = document.getElementById("collegename").value,
    el.buildName = document.getElementById("buildname").value;

    cleanTableRow();

    el.page = page;
    request(el,currencyFileHref,function(data){
        infoData = data.data.paginationList;
        pageSum = Math.ceil(data.data.total/el.rows);
        for(let i = 0; i < infoData.length; i ++){
            creatTableRow(infoData[i]);
        }
    },
    function(error){
        console.log(error);
    });

    nowpage.innerHTML = page;
    totalsum.innerHTML = pageSum;
}

//添加子节点
function addChild(parent, child, data){
	child.innerHTML = data;//添加内容
	parent.appendChild(child);//加入行
}

//动态绑定点击事件
function clickaction(){
	$("#tableBody ").on("click","td", function() {
        let stuid = this.parentNode;
        $('#myModal').modal('show');
        find.page = 1;
        find.rows = 1;
        find.stuNum = stuid.getElementsByTagName("td")[1].innerHTML;
        request(find,currencyFileHref,function(data){
            document.getElementById("Mtabtitle").innerHTML = "&nbsp;" + 
                data.data.paginationList[0].student.stuName + "&nbsp;的个人信息&nbsp;";

            document.getElementById("Mstuname").value = data.data.paginationList[0].student.stuName;
            document.getElementById("Msex").value = data.data.paginationList[0].student.sex;
            document.getElementById("Mstunum").value = data.data.paginationList[0].student.stuNum;
            document.getElementById("Mdepartname").value = data.data.paginationList[0].departName;
            document.getElementById("Mcollegename").value = data.data.paginationList[0].collegeName;
            document.getElementById("Mbuildname").value = data.data.paginationList[0].buildName;
            document.getElementById("Mdormitnum").value = data.data.paginationList[0].dormitNum;
            document.getElementById("Mbednum").value = data.data.paginationList[0].student.bedNum;
        },
        function(error){
            console.log(error);
        });

        find.id = '';
    });
}

// 修改
function confirmModify () {
    var hostUrl = 'http://39.108.175.18:8080/api/student.do?';
    var stuname = document.getElementById("Mstuname").value
    var stunum = document.getElementById("Mstunum").value;
    var sex = document.getElementById('Msex').value;
    var buildname = document.getElementById("Mbuildname").value;
    var dormitnum = document.getElementById("Mdormitnum").value;
    var bednum = document.getElementById("Mbednum").value;
    var urlto;
    var id;
    var departmentid;
    var domitid;

    el.page = 1;
    el.stuNum = stunum;
    request(el,currencyFileHref,function(data){
        id = data.data.paginationList[0].student.id;
        departmentid = data.data.paginationList[0].student.departmentId;
        domitid = data.data.paginationList[0].student.dormitId;
    },
    function(error){
        console.log(error);
    });

    urlto = 'id=' + id + '&stuNum=' + stunum
             + '&stuName=' + stuname + '&sex=' + sex
             + '&departmentId=' + departmentid + '&bedNum=' + bednum
             + '&dormitNum=' + dormitnum + '&buildName=' + buildname;

    $.ajax({
        url: hostUrl + urlto,
        type: 'put',
        datatype: 'json',
        cache: false,
        success: function (result,status,xhr) {
            console.log('success');
        },
        error: function (xhr,status,error) {
            alert(xhr.status);
        }
    });
    
}

//为表格添加行
function creatTableRow(data){
	let row = document.createElement('tr');//创建行
	let idCol = document.createElement('th');//创建1列

	let checkbox = document.createElement('input');
    checkbox.setAttribute("type", "checkbox");//设置属性
    checkbox.setAttribute("value", data.student.id);
    checkbox.setAttribute("name", "test");
    idCol.appendChild(checkbox);
    row.appendChild(idCol);

	let nameCol = document.createElement('td');//姓名
	let stuNumCol = document.createElement('td');//学号
	let sexCol = document.createElement('td');//性别
	let departNameCol = document.createElement('td');//专业
	let collegeNameCol = document.createElement('td');//院系
	let buildNameCol = document.createElement('td');//寝室楼
	let dormitNumCol = document.createElement('td');//寝室号

	addChild(row, nameCol, data.student.stuName);
	addChild(row, stuNumCol, data.student.stuNum);
	addChild(row, sexCol, data.student.sex);
	addChild(row, departNameCol, data.departName);
	addChild(row, collegeNameCol, data.collegeName);
	addChild(row, buildNameCol, data.buildName);
	addChild(row, dormitNumCol, data.dormitNum);

	tablebody.appendChild(row);
}

//清除表格内容
function cleanTableRow(){
    tablebody.innerHTML = '';
}

$(document).ready(function(){
    el.page=page;
    currencyFileHref = hostUrl + '/student/allInfo.do';
    request(el,currencyFileHref,function(data){
        infoData = data.data.paginationList;
        pageSum = Math.ceil(data.data.total/el.rows);
        for(let i=0; i<infoData.length; i++){
            creatTableRow(infoData[i]);
        }
        clickaction();
    },
    function(error){
        console.log(error);
    });

    nowpage.innerHTML = page;
    totalsum.innerHTML = pageSum;

    if(pageSum != 1){
    	laquo.onclick = function leftBtn(){
    		document.getElementById("rigBtn").classList.remove("disabled");
    		document.getElementById("rigBtn").classList.add("active");
    		if(document.getElementById("lefBtn").classList[0] === "active"){
    			page--;
    			if(page <= 1){
    				page = 1;
    				document.getElementById("lefBtn").classList.remove("active");
    				document.getElementById("lefBtn").classList.add("disabled");
    			}
    			nowpage.innerHTML = page;
    			//回退表格
                cleanTableRow();
                el.page=page;
                request(el,currencyFileHref,function(data){
                    infoData = data.data.paginationList;
                    //pageSum = Math.ceil(data.data.total/el.rows);
                    for(let i=0; i<infoData.length; i++){
                        creatTableRow(infoData[i]);
                    }
                },
                function(error){
                    console.log(error);
                });
    		}
    	}

    	raquo.onclick = function rightBtn(){
    		document.getElementById("lefBtn").classList.remove("disabled");
    		document.getElementById("lefBtn").classList.add("active");
    		if(document.getElementById("rigBtn").classList[0] === "active"){
    			page++;
    			if(page >= pageSum){
    				page = pageSum;
    				document.getElementById("rigBtn").classList.remove("active");
    				document.getElementById("rigBtn").classList.add("disabled");
    			}
    			nowpage.innerHTML = page;
    			//加载新内容
    			cleanTableRow();
                el.page=page;
                request(el,currencyFileHref,function(data){
                    infoData = data.data.paginationList;
                    //pageSum = Math.ceil(data.data.total/el.rows);
                    for(let i=0; i<infoData.length; i++){
                        creatTableRow(infoData[i]);
                    }
                },
                function(error){
                    console.log(error);
                });
    		}
    	}
    } else{
        document.getElementById("rigBtn").classList.add("disabled");
    }

    btn_del.onclick = function(){
        btnClikToDel();
    }

    btn_modify.onclick = function () {
        confirmModify();
    }

}())