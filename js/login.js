var hostUrl = 'http://39.108.175.18:8080/api/user/';

var input_username = $("#login #username");
var input_password = $("#login #password");
var btn_login = $("#login #btn_login")[0];
var warnInfo = $(".warnInfo")[0];
var username = input_username[0].value,
	password = input_password[0].value;

input_username[0].onblur = function () {
	username = input_username[0].value;
}

input_password[0].onblur = function () {
	password = input_password[0].value;
}

function confirmLogin () {
	var urlto = hostUrl + 'login.do?username=' + username + '&password=' + password;

	// hostUrl = 'http://39.108.175.18:8080/api/user/login.do?username=student&password=123456'
	var jqxhr = $.ajax(
		urlto, {
	    dataType: 'json',
	    async: true
	}).done(function (data) {
		// console.log(data);
	    if (data.data == 0) {
			warnInfo.innerHTML = "用户名或密码错误!";	
	    }
	    else if (data.data == 1) {
			window.location.href="isTeacher.html";
			localStorage.setItem('username',username);
	    }
	    else if (data.data == 2) {
	    	window.location.href="isStudent.html";
	    	localStorage.setItem('username',username);
	    }
	}).fail(function (xhr, status) {
    	ajaxLog('失败: ' + xhr.status + ', 原因: ' + status);
	})
}
btn_login.onclick = confirmLogin;