hostUrl = "http://39.108.175.18:8080/api";

function request(element,urlto,success,fail){
  if(element == null ){
    var element = {};
  }
  /*element.timestamp = new Date();
  element.encrypt_method = "md5";
  var jsondata = JSON.stringify(element);*/
  // console.log(element);
  $.ajax({
    type:"GET",
    url:urlto,
    data:element,
    dataType: 'json',
    async: false,
    //contentType:'application/json',
    success:function(data){
      success(data);
    },
    error:function(error){
      fail(error);
    }
  });
}

function requestToDel(urlto,success,fail){
  /*if(element == null ){element,
    var element = {};
  }
  element.timestamp = new Date();
  element.encrypt_method = "md5";
  var jsondata = JSON.stringify(element);*/
  console.log(urlto);
  $.ajax({
    type:"DELETE",
    url:urlto,
    //data:element,
    dataType: 'json',
    async: false,
    //contentType:'application/json',
    success:function(data){
      success(data);
    },
    error:function(error){
      fail(error);
    }
  });
}

function requestWithSession(element,urlto,success,fail,isAsync){
  if(localStorage.session){
    element.session = localStorage.session;
  }
  element.timestamp = new Date();
  element.encrypt_method = "md5";
  var jsondata = JSON.stringify(element);
  if(isAsync){
    $.ajax({
      type:"POST",
      url:urlto,
      data:jsondata,
      dataType: 'json',
      async: false,
      contentType:'application/json',
      success:function(data){
        success(data);
      },
      error:function(error){
        fail(error);
      }
    });
  }else {
    $.ajax({
      type:"POST",
      url:urlto,
      data:jsondata,
      dataType: 'json',
      contentType:'application/json',
      success:function(data){
        success(data);
      },
      error:function(error){
        fail(error);
      }
    });
  }
}